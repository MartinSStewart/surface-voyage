# Tasks

- Figure out a better way to move with touch screens
- Fade out geometry that's too close to the camera
- Add walls
  - Add function to find the shortest path between two points
- Add drawing sprites onto 3d surface
